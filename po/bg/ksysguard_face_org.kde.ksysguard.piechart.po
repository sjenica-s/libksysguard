# Bulgarian translations for libksysguard package.
# Copyright (C) 2022 This file is copyright:
# This file is distributed under the same license as the libksysguard package.
#
# Automatically generated, 2022.
# Mincho Kondarev <mkondarev@yahoo.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-12-09 00:44+0000\n"
"PO-Revision-Date: 2022-07-21 14:03+0200\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.04.3\n"

#: contents/ui/Config.qml:34
#, kde-format
msgid "Show Sensors Legend"
msgstr "Показване на легендата на сензорите"

#: contents/ui/Config.qml:38
#, kde-format
msgid "Start from Angle:"
msgstr "Започване от ъгъл:"

#: contents/ui/Config.qml:43
#, kde-format
msgctxt "angle degrees"
msgid "%1°"
msgstr "%1°"

#: contents/ui/Config.qml:46 contents/ui/Config.qml:59
#, kde-format
msgctxt "angle degrees"
msgid "°"
msgstr "°"

#: contents/ui/Config.qml:51
#, kde-format
msgid "Total Pie Angle:"
msgstr "Общ ъгъл на кръгова диаграма:"

#: contents/ui/Config.qml:56
#, kde-format
msgctxt "angle"
msgid "%1°"
msgstr "%1°"

#: contents/ui/Config.qml:64
#, kde-format
msgid "Rounded Lines"
msgstr "Заоблени линии"

#: contents/ui/Config.qml:69
#, kde-format
msgid "Automatic Data Range"
msgstr "Автоматичен обхват на данните"

#: contents/ui/Config.qml:73
#, kde-format
msgid "From:"
msgstr "От:"

#: contents/ui/Config.qml:80
#, kde-format
msgid "To:"
msgstr "До:"
