# Xavier Besnard <xavier.besnard@neuf.fr>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-07-03 00:18+0000\n"
"PO-Revision-Date: 2020-07-31 18:49+0200\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-i18n-doc@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.04.3\n"

#: contents/ui/Config.qml:24
#, kde-format
msgid "Group sensors based on the value of the total sensors."
msgstr ""
"Regrouper les senseurs à partir des valeurs de la totalité des senseurs."
