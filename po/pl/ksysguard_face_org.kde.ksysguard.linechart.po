# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the libksysguard package.
#
# Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-07 00:17+0000\n"
"PO-Revision-Date: 2021-01-16 11:39+0100\n"
"Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>\n"
"Language-Team: Polish <kde-i18n-doc@kde.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.1\n"

#: contents/ui/Config.qml:42
#, kde-format
msgid "Appearance"
msgstr "Wygląd"

#: contents/ui/Config.qml:47
#, kde-format
msgid "Show Sensors Legend"
msgstr "Pokaż legendę mierników"

#: contents/ui/Config.qml:51
#, kde-format
msgid "Stacked Charts"
msgstr "Nakładane wykresy"

#: contents/ui/Config.qml:55
#, kde-format
msgid "Smooth Lines"
msgstr "Gładkie linie"

#: contents/ui/Config.qml:59
#, kde-format
msgid "Show Grid Lines"
msgstr "Pokaż linie siatki"

#: contents/ui/Config.qml:63
#, kde-format
msgid "Show Y Axis Labels"
msgstr "Pokaż etykiety osi Y"

#: contents/ui/Config.qml:67
#, kde-format
msgid "Fill Opacity:"
msgstr "Wypełnij nieprzezroczystość:"

#: contents/ui/Config.qml:73
#, kde-format
msgid "Data Ranges"
msgstr "Zakresy danych"

#: contents/ui/Config.qml:78
#, kde-format
msgid "Automatic Y Data Range"
msgstr "Samoustalany zakres danych Y"

#: contents/ui/Config.qml:82
#, kde-format
msgid "From (Y):"
msgstr "Od (Y):"

#: contents/ui/Config.qml:89
#, kde-format
msgid "To (Y):"
msgstr "Do (Y):"

#: contents/ui/Config.qml:99
#, kde-format
msgid "Amount of History to Keep:"
msgstr "Ilość zapamiętywanej historii:"

#: contents/ui/Config.qml:102
#, kde-format
msgctxt "%1 is seconds of history"
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 sekunda"
msgstr[1] "%1 sekundy"
msgstr[2] "%1 sekund"
