msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-07 00:17+0000\n"
"PO-Revision-Date: 2022-11-19 14:48\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/libksysguard/ksysguard_face_org.kde."
"ksysguard.barchart.pot\n"
"X-Crowdin-File-ID: 12262\n"

#: contents/ui/Config.qml:35
#, kde-format
msgid "Show Sensors Legend"
msgstr "显示传感器图例"

#: contents/ui/Config.qml:39
#, kde-format
msgid "Stacked Bars"
msgstr "堆叠柱状图"

#: contents/ui/Config.qml:43
#, kde-format
msgid "Show Grid Lines"
msgstr "显示网格线"

#: contents/ui/Config.qml:47
#, kde-format
msgid "Show Y Axis Labels"
msgstr "显示 Y 轴标签"

#: contents/ui/Config.qml:51
#, kde-format
msgid "Automatic Data Range"
msgstr "自动数据范围"

#: contents/ui/Config.qml:55
#, kde-format
msgid "From:"
msgstr "来源："

#: contents/ui/Config.qml:62
#, kde-format
msgid "To:"
msgstr "目标："
