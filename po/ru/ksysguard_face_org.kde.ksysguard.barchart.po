# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the libksysguard package.
#
# Alexander Potashev <aspotashev@gmail.com>, 2020.
# Мария Шикунова <translation-team@basealt.ru>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-07 00:17+0000\n"
"PO-Revision-Date: 2021-10-13 12:09+0300\n"
"Last-Translator: Мария Шикунова <translation-team@basealt.ru>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 21.08.1\n"

#: contents/ui/Config.qml:35
#, kde-format
msgid "Show Sensors Legend"
msgstr "Показать легенду датчиков"

#: contents/ui/Config.qml:39
#, kde-format
msgid "Stacked Bars"
msgstr "Линейчатая с накоплением"

#: contents/ui/Config.qml:43
#, kde-format
msgid "Show Grid Lines"
msgstr "Показать сетку"

#: contents/ui/Config.qml:47
#, kde-format
msgid "Show Y Axis Labels"
msgstr "Показать метки на оси Y"

#: contents/ui/Config.qml:51
#, kde-format
msgid "Automatic Data Range"
msgstr "Автоматический диапазон значений"

#: contents/ui/Config.qml:55
#, kde-format
msgid "From:"
msgstr "От:"

#: contents/ui/Config.qml:62
#, kde-format
msgid "To:"
msgstr "До:"
